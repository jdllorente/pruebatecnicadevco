package blocdenotasonline.es.net.onlinenotepad.stepdefinitions;

import blocdenotasonline.es.net.onlinenotepad.configutations.SetTheStage;
import blocdenotasonline.es.net.onlinenotepad.tasks.AbrirPaginaInicio;
import blocdenotasonline.es.net.onlinenotepad.tasks.CrearNuevaNota;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class NotepadStepDefinitions {

    @Dado("que  {string} entre al sitio web de notepad")
    public void queEntreAlSitioWebDe(String actor) {
        SetTheStage.setTheStage();
        theActorCalled(actor).wasAbleTo(AbrirPaginaInicio.paginaInicio());
    }

    @Cuando("el suaurio cree una nueva nota  en  el portal de notepad")
    public void elSuaurioCreeUnaNuevaNotaEnElPortalDeNotepad() {
        theActorInTheSpotlight().attemptsTo(CrearNuevaNota.crearNuevaNota());
    }

    @Entonces("pueda verificar que la nota  fue creada correctamente .")
    public void puedaVerificarQueLaNotaFueCreadaCorrectamente() {

    }
}
