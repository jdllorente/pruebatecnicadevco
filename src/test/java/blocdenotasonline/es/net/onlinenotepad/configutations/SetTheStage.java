package blocdenotasonline.es.net.onlinenotepad.configutations;

import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.junit.Before;

public class SetTheStage {

    @Before
    public static void setTheStage() {

        OnStage.setTheStage(new OnlineCast());
    }

}
