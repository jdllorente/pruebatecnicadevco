package blocdenotasonline.es.net.onlinenotepad.tasks;

import blocdenotasonline.es.net.onlinenotepad.userinterfaces.HomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirPaginaInicio implements Task {
    HomePage paginaInicio;
    public static Performable paginaInicio(){
        return Tasks.instrumented(AbrirPaginaInicio.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(paginaInicio));

    }

}
