package blocdenotasonline.es.net.onlinenotepad.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.JavaScriptClick;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static blocdenotasonline.es.net.onlinenotepad.userinterfaces.HomePage.BOTON_NUEVA_NOTA;
import static blocdenotasonline.es.net.onlinenotepad.utilities.ConstantesWeb.NUMERO_SEGUNDOS_ESPERA;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class CrearNuevaNota implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                WaitUntil.the(BOTON_NUEVA_NOTA, isVisible()).forNoMoreThan(NUMERO_SEGUNDOS_ESPERA).seconds(),
                JavaScriptClick.on(BOTON_NUEVA_NOTA)
                );


    }

    public static CrearNuevaNota crearNuevaNota() {
        return new CrearNuevaNota();
    }
}