package blocdenotasonline.es.net.onlinenotepad.userinterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.online-notepad.net/es/bloc-de-notas-online")
public class HomePage extends PageObject {

    public static final Target BOTON_NUEVA_NOTA =Target.the("BOTON PARA  CREAR UNA NUEVA NOTA").locatedBy("//*[contains(text(),'Nueva nota')]");


}
